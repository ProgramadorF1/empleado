
public class Gerente extends Empleado{

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public double getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    public double getAporte() {
        return Aporte;
    }

    public void setAporte(double Aporte) {
        this.Aporte = Aporte;
    }
    
    private String sucursal;
    private double bonificacion;
    private double Aporte;
    
    public Gerente (){
        
    }
    
    public Gerente (String sc, double bon, double apo){
        super (" Diego "," Fernando ",19,9000000);
        this.sucursal = sc;
        this.bonificacion = bon;
        this.Aporte = apo;
    }
}
