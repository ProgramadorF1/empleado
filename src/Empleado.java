
public class Empleado {

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    public void Calculars(double r, double z, double x){
        this.salario = r+z-x;
    }
    
    public long Mostrars(){
        return (long) this.salario;
    }
    
    private String nombre;
    private String apellido;
    private int edad;
    private double salario;
    
    public Empleado(){
        
    }
    
    public Empleado (String n, String ap, int a, double sa){
        this.nombre = n;
        this.apellido = ap;
        this.edad = a;
        this.salario = sa;
    }
}
